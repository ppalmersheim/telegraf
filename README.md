# Telegraf installs

refactoring this to use docker for anything outside of a local host installation

## How to use

run `docker compose up -d` to build and start the container. this will launch with the `conf/telegraf.conf` and `/conf/networkdevices/*` loaded for telegraf.

The dockerfile will also pull in all mibs located in the `SNMP/` folder and place them in the right spot.

## todo

Need to make an ansible playbook for the install.
    the issue right now is that telegraf doesn't support arrays in environmental variables.